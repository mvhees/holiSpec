# holiSpec #
`holiSpec` is a technical language (a data format with a formal definition) for holistic computer-based collaboration and data exchange between ship building engineers and companies.

This data format is work in progress and result of the project [HOLISHIP](http://www.holiship.eu), which receives funding from the European Research Council (ERC) under the European Union's Horizon 2020 research and innovation programme under grant agreement no. 689074.


## How to contribute ##
- Development goes only through this platform, to streamline and unify the process and tool landscape
- Register at [Gitlab](https://gitlab.com). Using the new website design is recommended
- Install the popular distributed version control system (VCS) tool `Git`:
    - on Windows, download e.g. [this git client](https://github.com/git-for-windows/git/releases/tag/v2.13.3.windows.1) (hint: installer hung on one machine, it was necessary to kill one of the "git bash" commands in the process manager to continue); select option *Checkout Windows-style, commit Unix-style line endings* in the installer
    - on Linux, use your package manager to install Git (e.g. `sudo apt-get install git`). Configure Git to use generic line-endings: `git config core.autocrlf true`
- Extra: If you know how to use signed git commits, create a private/public key pair and upload the public key signature into your Gitlab profile, and set up git accordingly to make use of the `-S` option
- Being logged in, on the Gitlab project page [holiSpec](https://gitlab.com/dlr-sl/holiSpec), click **fork** and select your name to fork the project into your own space (create a full repository copy under your Gitlab account)
- Using the previously installed Git tool, **clone** the forked repository to your local computer (find the required Git URL on your cloned project page), e.g. `git clone https://gitlab.com/YOURNAME/holiSpec.git`
- Open an issue on the official *holiSpec* page under *Issues* for the problem you're having. If you start working on its solution yourself, add your name as the *Assignee*
- Pull and merge changes from the relevant upstream branch (e.g. master, develop, milestone, ...) before working on a feature: `git pull upstream *branchname*`
- Work on your local checkout it. You may also branch your local master by `git branch featurebranchname` and switch to that branch by `git checkout featurebranchname` to work on different features at different times. Learn the basics of Git to know what to do and how to merge branches
- Commit regularly and in feature-bound change sets
- Once done with a feature, sync your local repository back to your forked Gitlab project using `git push`. You may need to setup the `origin` alias to do so, and configure your name and email:
    - `git config [--global] user.email "email@mycompany.com"`  configures the email address to use for commit signing. Use `--global` to set for all your checked out projects
    - `git config [--global] user.name "My GitlabUserName"`
    - `git remote -v`  shows configured remote URLs
    - `git remote add upstream https://gitlab.com/dlr-sl/holiSpec`
    - `git remote set-url origin https://gitlab.com/YOURNAME/holiSpec`
- Create a pull request, or merge request (a request to download and integrate your contributions into the official repository) from your Gitlab repository to the [official repository](https://gitlab.com/dlr-sl/holiSpec.git) that you forked from (called *upstream*), by selecting *Merge Requests* on the top of your project page (or from the menu bar left), and choosing the appropriate source and target projects and branches. Don't forget to reference the issue number that you handled and solved (e.g. using `fixes #23` will automatically close issue 23 if the pull request is merged)
- Other community members may review the changes before allowing them to be merged into the offical repository


## Community process ##
- All additions and modifications must go through the issue tracker and a pull request. There is no secret or hidden addition to the standard
- To merge a pull request into the official repository, there should be established a dual control workflow
- For more complex decisions, ballots may be used, workshops held, and discussions on mailing lists be necessary, prior to merging something into the official repository
- Release roadmaps and milestones should be set up for certain feature sets with a specific deadline. This could lead to official versions like `V1.0` or `HOLISHIP_Final_2020` that users may use and reference in their internal or collaboration projects. The development of these feature releases should be done on git branches


## Tooling ##
The *holiSpec* definition is based on the XML standards, which allows a high degree of human readability while also supporting a wide range of software support.

To work with *holiSpec* files computationally, it is recommended to use existing tooling of a sibling project from the aviation sector, the [CPACS](http://cpacs.de) data format. It comes with a set of generic as well as specialized software libraries that may be used on Windows and Linux, from a range of programming languages:
- [TIXI](https://github.com/DLR-SC/tixi) a XML reader/writer library
- [TIGL](https://github.com/DLR-SC/tigl) a geometry operations tool that may be reused for certain data operations
- [RCE](http://rcenvironment.de) the software integration and collaboration framework that offers specially tuned integration support for XML data (originally for *CPACS*, but also extended to *holiSpec*)


## Relationship with software integration ##
The *holiSpec* data format serves as a parametric description of a ship design, including the vessel and mission.
The file may contain additional information like design constraints, requirements, further environmental data required by tools that work with the dataset.
It always represents a certain immutable state of a design and can be used as a dataset being passed around between optimization, simulation and analysis tools inside a design workflow.

In contrast to standalone solutions, with holiSpec in RCE there is no central database that allows asynchronous modification; the updated versions of the dataset are passed between tools and may be merged into a final output.
The outcome of such a workflow can be a final optimized dataset with a resulting ship design, or just a variant computed for comparison with other design studies.

A tutorial in setting up and maintaining the RCE platfrom can be found [here](docs/rce-tutorial-configuration.md).
